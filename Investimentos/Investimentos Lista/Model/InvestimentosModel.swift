//
//  InvestimentosModel.swift
//  BTG Digital - Teste iOS
//
//  Created by Daniel Griso Filho on 5/16/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import Foundation
import ObjectMapper

class InvestimentosModel: Mappable {
    
    var id: Double?
    var product: String?
    var description: String?
    var type: String?
    var riskLevel: Int?
    var riskName: String?
    var investedBalance: Double?
    var availableAccountBalance: AnyObject?
    var minimumInitialInvestment: Double?
    var timeLimit: String?
    var profitabilityDay: Double?
    var profitabilityLastMonth: Double?
    var profitabilityMonth: Double?
    var profitabilityYear: Double?
    var profitabilityTwelveMonths: Double?
    var profitabilityCotaLastMonth: Bool?
    var tipoInvestimentoIndicador: String?
    var detailMinimumInitialInvestiment: Double?
    var detailMinimumMoviment: Double?
    var detailMinimumBalanceRemain: Double?
    var detailAdministrationFee: Double?
    var detailPerformanceFee: AnyObject?
    var detailDateQuota: String?
    var detailValueQuota: Double?
    var detailNumberOfDaysFinancialInvestment: Double?
    var detailInvestimentQuota: String?
    var detailInvestmentFinancialSettlement: String?
    var detailRescueQuota: String?
    var detailRescuefinancialSettlement: String?
    var detailHourInvestimentRescue: String?
    var detailAnbimaRating: String?
    var detailAnbimaCode: String?
    var detailCategoryDescription: String?
    var detailCategoryCode: Double?
    var detailCustody: String?
    var detailAuditing: AnyObject?
    var detailManager: String?
    var detailAdministrator: String?
    var detailUpdateDate: String?
    var detailQuotaRule: AnyObject?
    var detailFiles: [[String: AnyObject]]?
    var detailFilesId: Double?
    var detailFilesTypeId: Double?
    var detailFilesVersion: AnyObject?
    var detailFilesName: String?
    var detailFilesFormat: String?
    var detailFilesDescription: String?
    var detailFilesTypeCode: String?
    var detailFilesSize: AnyObject?
    var detailFilesMimeType: AnyObject?
    var detailFilesFile: AnyObject?
    var investmentMainDisclaimer: String?
    var investmentConfirmationDisclaimer: String?
    var investmentRequestDate: String?
    var investmentAciValidDate: String?
    var investmentValue: AnyObject?
    var investmentMaxInvestmentDate: String?
    var investmentQuotaDate: AnyObject?
    var investmentSettlementDate: AnyObject?
    var investmentAciErrorCode: AnyObject?
    var assetCode: Double?
    var netEquity: Double?
    var averageNetEquity: AnyObject?
    var begin: String?
    var inAtivo: Bool?
    var signed: Bool?
    var scheduled: Bool?
    var selected: Bool?
    var issuerName: String?
    var issuerId: Double?
    var quotaRule: AnyObject?
    var externalIssuer: String?
    var isRecentFund: Bool?
    var nextDateUtil: AnyObject?
    var existOperation: Bool?
    var signDocuments: AnyObject?
    var fundsBenchmarks: AnyObject?
    var graphs: AnyObject?
    var profitabilityHistory: AnyObject?
    var showDetail: Bool?
    var grantLevel: Bool?
    var isBlackList: Bool?
    var isWhiteList: Bool?
    
    required init(from decoder: Decoder) throws {
        
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        product <- map["product"]
        description <- map["description"]
        type <- map["type"]
        riskLevel <- map["riskLevel"]
        riskName <- map["riskName"]
        investedBalance <- map["investedBalance"]
        availableAccountBalance <- map["availableAccountBalance"]
        minimumInitialInvestment <- map["minimumInitialInvestment"]
        timeLimit <- map["timeLimit"]
        profitabilityDay <- map["profitability.day"]
        profitabilityLastMonth <- map["profitability.lastMonth"]
        profitabilityMonth <- map["profitability.month"]
        profitabilityYear <- map["profitability.year"]
        profitabilityTwelveMonths <- map["profitability.twelveMonths"]
        profitabilityCotaLastMonth <- map["profitability.cotaLastMonth"]
        tipoInvestimentoIndicador <- map["tipoInvestimentoIndicador"]
        detailMinimumInitialInvestiment <- map["detail.minimumInitialInvestiment"]
        detailMinimumMoviment <- map["detail.minimumMoviment"]
        detailMinimumBalanceRemain <- map["detail.minimumBalanceRemain"]
        detailAdministrationFee <- map["detail.administrationFee"]
        detailPerformanceFee <- map["detail.performanceFee"]
        detailDateQuota <- map["detail.dateQuota"]
        detailValueQuota <- map["detail.valueQuota"]
        detailNumberOfDaysFinancialInvestment <- map["detail.numberOfDaysFinancialInvestment"]
        detailInvestimentQuota <- map["detail.investimentQuota"]
        detailInvestmentFinancialSettlement <- map["detail.investmentFinancialSettlement"]
        detailRescueQuota <- map["detail.rescueQuota"]
        detailRescuefinancialSettlement <- map["detail.rescuefinancialSettlement"]
        detailHourInvestimentRescue <- map["detail.hourInvestimentRescue"]
        detailAnbimaRating <- map["detail.anbimaRating"]
        detailAnbimaCode <- map["detail.anbimaCode"]
        detailCategoryDescription <- map["detail.categoryDescription"]
        detailCategoryCode <- map["detail.categoryCode"]
        detailCustody <- map["detail.custody"]
        detailAuditing <- map["detail.auditing"]
        detailManager <- map["detail.manager"]
        detailAdministrator <- map["detail.administrator"]
        detailUpdateDate <- map["detail.updateDate"]
        detailQuotaRule <- map["detail.quotaRule"]
        detailFiles <- map["detail.files"]
        investmentMainDisclaimer <- map["investment.mainDisclaimer"]
        investmentConfirmationDisclaimer <- map["investment.confirmationDisclaimer"]
        investmentRequestDate <- map["investment.requestDate"]
        investmentAciValidDate <- map["investment.aciValidDate"]
        investmentValue <- map["investment.value"]
        investmentMaxInvestmentDate <- map["investment.maxInvestmentDate"]
        investmentQuotaDate <- map["investment.quotaDate"]
        investmentSettlementDate <- map["investment.settlementDate"]
        investmentAciErrorCode <- map["investment.aciErrorCode"]
        assetCode <- map["assetCode"]
        netEquity <- map["netEquity"]
        averageNetEquity <- map["averageNetEquity"]
        begin <- map["begin"]
        inAtivo <- map["inAtivo"]
        signed <- map["signed"]
        scheduled <- map["scheduled"]
        selected <- map["selected"]
        issuerName <- map["issuerName"]
        issuerId <- map["issuerId"]
        quotaRule <- map["quotaRule"]
        externalIssuer <- map["externalIssuer"]
        isRecentFund <- map["isRecentFund"]
        nextDateUtil <- map["nextDateUtil"]
        existOperation <- map["existOperation"]
        fundsBenchmarks <- map["fundsBenchmarks"]
        graphs <- map["graphs"]
        profitabilityHistory <- map["profitabilityHistory"]
        showDetail <- map["showDetail"]
        grantLevel <- map["grantLevel"]
        isBlackList <- map["isBlackList"]
        isWhiteList <- map["isWhiteList"]
    }
    
}
