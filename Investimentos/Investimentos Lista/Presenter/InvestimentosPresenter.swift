//
//  InvestimentosPresenter.swift
//  BTG Digital - Teste iOS
//
//  Created by Daniel Filho on 17/05/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import Foundation

protocol InvestimentosPresenterProtocol {
    func resultFromRequest(arrayFunds:[InvestimentosModel])
    func showAlert(title: String, message: String)
}

class InvestimentosPresenter {
    
    var presenter: InvestimentosPresenterProtocol!
    var interactor: InvestimentosInteractor!
    
    init(presenter: InvestimentosPresenterProtocol) {
        self.presenter = presenter
        self.interactor = InvestimentosInteractor(interactor: self)
    }
    
    func getAllFunds() {
        self.interactor.getFunds()
    }
}

extension InvestimentosPresenter: InvestimentosInteractorProtocol {
    func fetchFunds(arrayFunds: [InvestimentosModel]) {
        if arrayFunds.count < 0 {
            self.presenter.showAlert(title: "Error", message: "There are no countries in this resuqest API")
        }else{
            self.presenter.resultFromRequest(arrayFunds: arrayFunds)
        }
    }
}
