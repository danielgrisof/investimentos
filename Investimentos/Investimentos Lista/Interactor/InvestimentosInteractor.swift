//
//  InvestimentosInteractor.swift
//  BTG Digital - Teste iOS
//
//  Created by Daniel Griso Filho on 5/16/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

protocol InvestimentosInteractorProtocol {
    func fetchFunds(arrayFunds: [InvestimentosModel])
}

class InvestimentosInteractor {
    private var interactor: InvestimentosInteractorProtocol!
    private let url = URL(string: "https://www.btgpactualdigital.com/services/public/funds/")
    
    required init(interactor: InvestimentosInteractorProtocol) {
        self.interactor = interactor
    }
    
    func getFunds() {
        guard let safeUrl = url else {fatalError("Bad URL")}
        request(safeUrl).responseArray { (response: DataResponse<[InvestimentosModel]>) in
            guard let fundsArray = response.result.value else{fatalError("No Result Comming from JSON")}
            self.interactor.fetchFunds(arrayFunds: fundsArray)
        }
    }
}
