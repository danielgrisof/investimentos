//
//  InvestimentosCollectionViewController.swift
//  BTG Digital - Teste iOS
//
//  Created by Daniel Griso Filho on 5/16/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import UIKit

private let reuseIdentifierCell = "Cell"
private let reuseIdentifierHeader = "headerCell"

class InvestimentosCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    //MARK: - Properts and Variables
    var funds = [InvestimentosModel]()
    var presenter: InvestimentosPresenter!
    let searchController = UISearchController(searchResultsController: nil)
    var searchFunds = [InvestimentosModel]()
    var isOpen = false
    var section:Int?
    var preSection:Int?
    
    //MARK: - Life Cicle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCustomCells()
        self.setupNavigationPreferences()
        self.presenter = InvestimentosPresenter(presenter: self)
        self.presenter.getAllFunds()
    }
    
    // MARK: - Internal Functions
    func registerCustomCells() {
        self.collectionView!.register(UINib(nibName: "InvestimentosCustomCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifierCell)
        
        self.collectionView!.register(UINib(nibName: "InvestimentosCustomHeaderCell", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: reuseIdentifierHeader)
    }
    
    func setupNavigationPreferences() {
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backButton
    }
    

    @IBAction func openFilter(_ sender: UIBarButtonItem) {
        self.setupSearchController()
    }
    
    @IBAction func openSearch(_ sender: UIBarButtonItem) {
        self.setupSearchController()
    }
    // MARK: - UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            if let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: reuseIdentifierHeader, for: indexPath) as? InvestimentosCustomHeaderCell {
                return headerView
            }else{
                return InvestimentosCustomHeaderCell()
            }
        default:
            assert(false,"Unexpected element kind")
        }
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFiltering() {
            return searchFunds.count
        }
        return funds.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierCell, for: indexPath) as? InvestimentosCustomCell {
            cell.cellProtocol = self
            cell.viewButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi/4)
            if isFiltering(){
                cell.configureCell(with: [searchFunds[indexPath.row]],isOpen: isOpen)
            }else{
                cell.configureCell(with: [funds[indexPath.row]],isOpen: isOpen)
            }
            return cell
        }else{
            return InvestimentosCustomCell()
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.size.width - 20
        if self.isOpen, let row = self.section, row == indexPath.row  {
            return CGSize(width: width, height: 471)
            
        }else{
            return CGSize(width: width, height: 183)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(integerLiteral: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let width = UIScreen.main.bounds.size.width
        return CGSize(width: width, height: 154)
    }
    
    
}

//MARK: - Extension of the class to implement project Protocols
extension InvestimentosCollectionViewController: InvestimentosPresenterProtocol, InvestimentosCustomCellProtocol {
    
    //MARK: - InvestimentosPresenterProtocol
    func resultFromRequest(arrayFunds: [InvestimentosModel]) {
        self.funds = arrayFunds
        self.collectionView?.reloadData()
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let firstButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(firstButton)
        present(alertController, animated: true, completion: nil)
    }
    
    //MARK: - InvestimentosCustomCellProtocol
    func openFullCell(cell: InvestimentosCustomCell) {
        
        let indexPath = collectionView?.indexPath(for: cell)
        if self.section != nil {
            self.preSection = self.section
        }
        
        self.section = indexPath?.row
        
        if self.preSection == self.section{
            self.preSection = nil
            self.section = nil
        }else if self.preSection != nil {
            self.isOpen = false
        }
        
        if isOpen {
            UIView.animate(withDuration: 0.6) {
                cell.viewButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi/4)
            }
        }else{
            UIView.animate(withDuration: 0.6) {
                cell.viewButton.transform = .identity
            }
        }
        
        self.isOpen = !self.isOpen
        
        self.collectionView?.reloadItems(at: (collectionView?.indexPathsForSelectedItems)!)
    }
}

//MARK: - Extension of the class to implement system Protocols
extension InvestimentosCollectionViewController: UISearchResultsUpdating, UISearchBarDelegate {
    
    //MARK: - Configurate SearchController e Search Bar
    func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Pesquisar"
        definesPresentationContext = true
        searchController.searchBar.barStyle = .default
        searchController.searchBar.scopeButtonTitles = ["Todos","CONSERVADOR", "MODERADO", "BALANCEADO", "CRESCIMENTO", "SOFISTICADO"]
        searchController.searchBar.delegate = self
        searchController.searchBar.backgroundColor = UIColor.lightGray
        searchController.searchBar.sizeToFit()
        present(searchController, animated: true, completion: nil)
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        self.searchFunds = funds.filter({ (funds: InvestimentosModel) -> Bool in
            let doesCategoryMath = (scope == "All") || (funds.riskName == scope)
            
            if searchBarIsEmpty(){
                return doesCategoryMath
            }else{
                return (funds.detailCategoryDescription?.lowercased().contains(searchText.lowercased()))!
            }
        })
        self.collectionView?.reloadData()
    }
    
    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
    
    // MARK: - UISearchResultsUpdating, UISearchBarDelegate
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        self.filterContentForSearchText(searchController.searchBar.text!, scope: scope)
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        self.filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
}
