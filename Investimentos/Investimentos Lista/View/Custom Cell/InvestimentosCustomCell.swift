//
//  InvestimentosCustomCell.swift
//  BTG Digital - Teste iOS
//
//  Created by Daniel Griso Filho on 5/16/18.
//  Copyright © 2018 Daniel Griso Filho. All rights reserved.
//

import UIKit

protocol InvestimentosCustomCellProtocol{
    func openFullCell(cell: InvestimentosCustomCell)
}


class InvestimentosCustomCell: UICollectionViewCell {
    
    var cellProtocol: InvestimentosCustomCellProtocol?
    
    @IBOutlet weak var viewCorRisco: UIView!
    
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var lblSubtitulo: UILabel!
    @IBOutlet weak var lblPorcentagemMes: UILabel!
    @IBOutlet weak var lblAplicacaoInicialValor: UILabel!
    @IBOutlet weak var lblResgateValor: UILabel!
    @IBOutlet weak var lblMesAtualValor: UILabel!
    @IBOutlet weak var lblAnoValor: UILabel!
    @IBOutlet weak var lblInicioFundoData: UILabel!
    @IBOutlet weak var lblPatrimonioValor: UILabel!
    @IBOutlet weak var lblUtimaCotaData: UILabel!
    @IBOutlet weak var lblValorCota: UILabel!
    @IBOutlet weak var lblPorcentagemDozeMeses: UILabel!
    
    @IBOutlet weak var viewButton: UIView!
    @IBOutlet weak var buttonClose: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    @IBAction func btnOpenCloseCell(_ sender: UIButton) {
        
        self.cellProtocol?.openFullCell(cell: self)
    }
    
    func configureCell(with obj:[InvestimentosModel],isOpen: Bool){
        
        for funds in obj {
            self.setupInformationsCell(with: funds)
        }
        self.buttonClose.tintColor = UIColor.white
        self.viewButton.backgroundColor = UIColor.lightGray
    }
    
    func setupInformationsCell(with funds: InvestimentosModel){
        let percentFormater = NumberFormatter()
        percentFormater.numberStyle = .percent
        percentFormater.multiplier = 1.00
        percentFormater.maximumFractionDigits = 2
        let currenciFormater = NumberFormatter()
        currenciFormater.numberStyle = .currency
        currenciFormater.locale = Locale.current
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        
        if let viewRisco = funds.riskName {
            switch viewRisco {
            case "CRESCIMENTO":
                self.viewCorRisco.backgroundColor = UIColor.orange
            case "SOFISTICADO":
                self.viewCorRisco.backgroundColor = UIColor.red
            case "MODERADO":
                self.viewCorRisco.backgroundColor = UIColor.green
            case "BALANCEADO":
                self.viewCorRisco.backgroundColor = UIColor.brown
            case "CONSERVADOR":
                self.viewCorRisco.backgroundColor = UIColor.blue
            default:
                self.viewCorRisco.backgroundColor = UIColor.black
            }
        }
        
        if let product = funds.product {
            self.lblTitulo.text = product
        }else{
            self.lblTitulo.text = "--"
        }
        
        if let categoryDescription = funds.detailCategoryDescription{
            self.lblSubtitulo.text = categoryDescription
        }else{
            self.lblSubtitulo.text = "--"
        }
        
        if let percentTwelveMonths = funds.profitabilityTwelveMonths {
            self.lblPorcentagemDozeMeses.text = percentFormater.string(from: NSNumber(value: percentTwelveMonths))
        }else{
            self.lblPorcentagemDozeMeses.text = "--"
        }
        
        if let minimumInvestiment = funds.minimumInitialInvestment{
            self.lblAplicacaoInicialValor.text = currenciFormater.string(from: NSNumber(value: minimumInvestiment))
        }else{
            self.lblAplicacaoInicialValor.text = "--"
        }
        
        if let rescueQuota = funds.detailRescueQuota {
            self.lblResgateValor.text = rescueQuota
        }else{
            self.lblResgateValor.text = "--"
        }
        
        if let percentMonth = funds.profitabilityMonth{
            self.lblMesAtualValor.text = percentFormater.string(from: NSNumber(value: percentMonth))
        }else{
            self.lblMesAtualValor.text = "--"
        }
        
        if let percentYear = funds.profitabilityYear {
            self.lblAnoValor.text = percentFormater.string(from: NSNumber(value: percentYear))
        }else{
            self.lblAnoValor.text = "--"
        }
        
        if let beginFunds = funds.begin {
            let date = dateFormatter.date(from: beginFunds)
            dateFormatter.dateFormat = "dd/MM/yyyy"
            guard let safeDate = date else{return}
            self.lblInicioFundoData.text = dateFormatter.string(from: safeDate)
        }else{
            self.lblInicioFundoData.text = "--"
        }
        
        if let lastQuota = funds.detailDateQuota {
            let date = dateFormatter.date(from: lastQuota)
            dateFormatter.dateFormat = "dd/MM/yyyy"
            guard let safeDate = date else{return}
            self.lblUtimaCotaData.text = dateFormatter.string(from: safeDate)
        }else{
            self.lblUtimaCotaData.text = "--"
        }
        
        if let quotaValue = funds.detailValueQuota {
            self.lblValorCota.text = currenciFormater.string(from: NSNumber(value: quotaValue))
        }else{
            self.lblValorCota.text = "--"
        }
    }

}
